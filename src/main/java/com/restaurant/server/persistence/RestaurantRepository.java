package com.restaurant.server.persistence;

import java.util.List;

import org.mongodb.morphia.Datastore;

import com.restaurant.server.domain.Restaurant;
import com.restaurant.server.utilities.InvalidParamException;
import com.restaurant.server.utilities.NotFoundException;

public class RestaurantRepository {

	private static Datastore datastore = DatastoreManager.getInstance().getDatastore();

	public static void saveRestaurant(Restaurant restaurant) throws InvalidParamException {
		if (restaurant == null)
			throw new InvalidParamException();
		datastore.save(restaurant);
	}

	public static List<Restaurant> getAllRestaurants() {
		return datastore.find(Restaurant.class).asList();
	}

	public static Restaurant getRestaurant(String restaurantId) throws NotFoundException {
		Restaurant r = datastore.find(Restaurant.class).field("id").equal(restaurantId).get();

		if (r == null)
			throw new NotFoundException();
		return r;
	}
}
