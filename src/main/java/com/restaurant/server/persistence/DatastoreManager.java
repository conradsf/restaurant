package com.restaurant.server.persistence;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

public class DatastoreManager {

    private static DatastoreManager instance=new DatastoreManager();
    private Datastore datastore;
    
    private DatastoreManager() {
        try {
            MongoClient mongo = new MongoClient("localhost");    
            this.datastore=new Morphia().createDatastore(mongo, "restaurant");            
            datastore.ensureIndexes();            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static DatastoreManager getInstance(){        
        return instance;
    }
    
    public Datastore getDatastore(){        
        return datastore;
    }

 

}