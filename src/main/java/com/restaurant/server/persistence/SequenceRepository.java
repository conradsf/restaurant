package com.restaurant.server.persistence;

import org.mongodb.morphia.Datastore;

public class SequenceRepository {

	private static Datastore datastore = DatastoreManager.getInstance().getDatastore();

	public static String getNextRestaurantId() {
		Sequence seq = datastore.find(Sequence.class).field("key").equal("restaurants").get();

		if (seq == null) {
			seq = new Sequence("restaurants");
		}

		String id = seq.getCounter();
		seq.increment();

		datastore.save(seq);
		return id;
	}

	public static String getNextTableId() {
		Sequence seq = datastore.find(Sequence.class).field("key").equal("tables").get();

		if (seq == null) {
			seq = new Sequence("tables");
		}

		String id = seq.getCounter();
		seq.increment();

		datastore.save(seq);
		return id;
	}
}
