package com.restaurant.server.domain;

import org.mongodb.morphia.annotations.Embedded;

import com.restaurant.server.application.dto.TableDTO;
import com.restaurant.server.utilities.InvalidParamException;
@Embedded
public class Table {

	private String id;
	private int capacity;
	private boolean isBooked = false;

	public Table(String id, TableDTO tableDTO) throws InvalidParamException {
		if (tableDTO == null)
			throw new InvalidParamException();
		if (id.equals(""))
			throw new InvalidParamException();
		this.id = id;
		this.capacity = tableDTO.getCapacity();
		this.isBooked = tableDTO.getIsBooked();
	}
	
	public Table(){
	
	}

	public int getCapacity() {
		return this.capacity;
	}

	public boolean getIsBooked() {
		return this.isBooked;
	}
	
	public void switchBookTable() {
		this.isBooked = !this.isBooked;
	}

	public String getId() {
		return this.id;
	}

	public void updateTable(TableDTO tableDTO) throws InvalidParamException {
		if (tableDTO == null)
			throw new InvalidParamException();
		this.isBooked = tableDTO.getIsBooked();
	}

}