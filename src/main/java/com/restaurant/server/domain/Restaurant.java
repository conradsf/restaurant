package com.restaurant.server.domain;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import com.restaurant.server.application.dto.RestaurantDTO;
import com.restaurant.server.utilities.InvalidParamException;
import com.restaurant.server.utilities.NotFoundException;

@Entity("restaurants")
public class Restaurant {
	@Id
	private String id;
	private String name;
	@Embedded
	private List<Table> tables = new ArrayList<Table>();

	public Restaurant() {
	}

	public Restaurant(String id, RestaurantDTO restaurantDTO) throws InvalidParamException {
		if (restaurantDTO == null)
			throw new InvalidParamException();
		if (id.equals(""))
			throw new InvalidParamException();

		this.name = restaurantDTO.getName();
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public List<Table> getAllTables() {
		return this.tables;
	}

	public void addTable(Table table) throws InvalidParamException {
		if (table == null)
			throw new InvalidParamException();
		this.tables.add(table);
	}

	public Table getTable(String tableId) throws NotFoundException {
		for (Table table : tables) {
			if (table.getId().equals(tableId))
				return table;
		}
		throw new NotFoundException();
	}

}
