package com.restaurant.server.utilities;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, reason = "Invalid information")
public class InvalidParamException extends Exception {

	public InvalidParamException() {
		super("Invalid param error");
	}
}
