package com.restaurant.server.application;

import java.util.ArrayList;
import java.util.List;

import com.restaurant.server.application.dto.TableDTO;
import com.restaurant.server.domain.Restaurant;
import com.restaurant.server.domain.Table;
import com.restaurant.server.persistence.RestaurantRepository;
import com.restaurant.server.persistence.SequenceRepository;
import com.restaurant.server.utilities.InvalidParamException;
import com.restaurant.server.utilities.NotFoundException;

public class TableController {

	public TableDTO createTable(String restaurantId, TableDTO tableDTO)
			throws InvalidParamException, NotFoundException {
		Restaurant restaurant = RestaurantRepository.getRestaurant(restaurantId);
		String tableId = SequenceRepository.getNextTableId();

		Table table = new Table(tableId, tableDTO);
		restaurant.addTable(table);

		RestaurantRepository.saveRestaurant(restaurant);
		return new TableDTO(table);
	}

	public List<TableDTO> getAllTables(String restaurantId) throws NotFoundException, InvalidParamException {
		Restaurant restaurant = RestaurantRepository.getRestaurant(restaurantId);
		return convertTablesToDTO(restaurant.getAllTables());
	}

	public TableDTO getTable(String restaurantId, String tableId) throws NotFoundException, InvalidParamException {
		Restaurant restaurant = RestaurantRepository.getRestaurant(restaurantId);
		Table table = restaurant.getTable(tableId);
		return new TableDTO(table);
	}

	private List<TableDTO> convertTablesToDTO(List<Table> allTables) throws InvalidParamException {
		List<TableDTO> result = new ArrayList<TableDTO>();
		for (Table table : allTables) {
			result.add(new TableDTO(table));
		}
		return result;
	}

	public TableDTO bookTable(String restaurantId, String tableId) throws NotFoundException, InvalidParamException {
		Restaurant restaurant = RestaurantRepository.getRestaurant(restaurantId);
		Table table = restaurant.getTable(tableId);
		table.switchBookTable();

		RestaurantRepository.saveRestaurant(restaurant);
		return new TableDTO(table);
	}

}