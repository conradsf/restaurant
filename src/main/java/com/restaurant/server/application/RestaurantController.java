package com.restaurant.server.application;

import java.util.ArrayList;
import java.util.List;

import com.restaurant.server.application.dto.RestaurantDTO;
import com.restaurant.server.domain.Restaurant;
import com.restaurant.server.persistence.RestaurantRepository;
import com.restaurant.server.persistence.SequenceRepository;
import com.restaurant.server.utilities.InvalidParamException;
import com.restaurant.server.utilities.NotFoundException;

public class RestaurantController {
	
	public RestaurantDTO createRestaurant(RestaurantDTO restaurantDTO) throws InvalidParamException {
		String restaurantId = SequenceRepository.getNextRestaurantId();
		Restaurant restaurant = new Restaurant(restaurantId, restaurantDTO);

		RestaurantRepository.saveRestaurant(restaurant);
		return new RestaurantDTO(restaurant);
	}
	
	public RestaurantDTO getRestaurant(String restaurantId) throws InvalidParamException, NotFoundException {
		Restaurant restaurant = RestaurantRepository.getRestaurant(restaurantId);
		return new RestaurantDTO(restaurant);
	}

	public List<RestaurantDTO> getAllRestaurants() throws InvalidParamException {
		List<Restaurant> restaurants = RestaurantRepository.getAllRestaurants();
		List<RestaurantDTO> result = new ArrayList<RestaurantDTO>();
		for (Restaurant r : restaurants) {
			result.add(new RestaurantDTO(r));
		}
		return result;
	}

}
