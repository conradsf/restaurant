package com.restaurant.server.application.dto;

import com.restaurant.server.domain.Table;
import com.restaurant.server.utilities.InvalidParamException;

public class TableDTO {
	private String id;
	private int capacity;
	private boolean isBooked;

	public TableDTO(Table table) throws InvalidParamException {
		if (table == null)
			throw new InvalidParamException();
		this.id = table.getId();
		this.capacity = table.getCapacity();
		this.isBooked = table.getIsBooked();
	}

	public int getCapacity() throws InvalidParamException {
		if (this.capacity < 1)
			throw new InvalidParamException();
		return this.capacity;
	}
	
	public boolean getIsBooked() {
		return this.isBooked;
	}
}
