package com.restaurant.server.application.dto;

import com.restaurant.server.domain.Restaurant;
import com.restaurant.server.utilities.InvalidParamException;

public class RestaurantDTO {

	private String id;
	private String name;

	public RestaurantDTO(Restaurant restaurant) throws InvalidParamException {
		if (restaurant == null)
			throw new InvalidParamException();
		this.id = restaurant.getId();
		this.name = restaurant.getName();
	}

	public String getName() throws InvalidParamException {
		if (name.equals(""))
			throw new InvalidParamException();
		return this.name;
	}
}
