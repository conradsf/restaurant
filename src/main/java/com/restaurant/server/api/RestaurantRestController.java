package com.restaurant.server.api;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.restaurant.server.application.RestaurantController;
import com.restaurant.server.application.dto.RestaurantDTO;
import com.restaurant.server.utilities.InvalidParamException;

import com.google.gson.Gson;

@CrossOrigin
@RestController
@RequestMapping("/restaurants")
public class RestaurantRestController {
	
	private RestaurantController controller = new RestaurantController();

	@PostMapping
	public String createRestaurant(@RequestBody String jRestaurant) throws InvalidParamException {
		RestaurantDTO restaurant = new Gson().fromJson(jRestaurant, RestaurantDTO.class);

		restaurant = controller.createRestaurant(restaurant);

		return new Gson().toJson(restaurant);
	}
}
