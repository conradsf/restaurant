package com.restaurant.server.api;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.restaurant.server.application.TableController;
import com.restaurant.server.application.dto.TableDTO;
import com.restaurant.server.utilities.InvalidParamException;
import com.restaurant.server.utilities.NotFoundException;
import com.google.gson.Gson;

@CrossOrigin
@RestController
@RequestMapping("/restaurants/{restaurantId}/tables")
public class TableRestController {

	private TableController controller = new TableController();
	
	@PostMapping
	public String createTable(@PathVariable String restaurantId, @RequestBody String jTable)
			throws InvalidParamException, NotFoundException {
		TableDTO table = new Gson().fromJson(jTable, TableDTO.class);
		table = controller.createTable(restaurantId, table);
		return new Gson().toJson(table);
	}
	
	@GetMapping
	public String getAllTables(@PathVariable String restaurantId) throws NotFoundException, InvalidParamException {
		List<TableDTO> tables = controller.getAllTables(restaurantId);
		return new Gson().toJson(tables);
	}
	
	@PostMapping("/{tableId}")
	public String bookTable(@PathVariable String restaurantId, @PathVariable String tableId)
			throws NotFoundException, InvalidParamException {
		TableDTO table = controller.bookTable(restaurantId, tableId);
		return new Gson().toJson(table);
	}

}
